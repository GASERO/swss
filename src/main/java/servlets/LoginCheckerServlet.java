package servlets;

import org.json.JSONArray;
import org.json.JSONObject;
import services.ServicesService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginCheckerServlet extends HttpServlet {
    private UserService userService;
    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*JSONObject jsonObject = new JSONObject();
        jsonObject.put("flag2", userService.check(request.getParameter("login")));
        try {
            System.out.println(jsonObject.toString());
            response.getWriter().println(jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        response.getWriter().println(userService.check(request.getParameter("login")));

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/account");
    }
}
