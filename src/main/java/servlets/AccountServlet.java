package servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import helpers.ConfigSingleton;
import models.User;
import services.ServicesService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;


public class AccountServlet extends HttpServlet {
    UserService userService;
    ServicesService servicesService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        servicesService = new ServicesService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("currentUser");
        user.setFirstName(request.getParameter("firstName"));
        user.setSecondName(request.getParameter("secondName"));
        user.setPhone(request.getParameter("phone")); ;
        user.setDescription(request.getParameter("description") );
        user.setProfession(request.getParameter("profession"));
        user.setCollege(request.getParameter("college"));
        user.setDateFinish(Integer.parseInt(request.getParameter("dateFinish")));
        request.getSession().setAttribute("currentUser",user);
        userService.update(user);
        doGet(request, response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user =(User) request.getSession().getAttribute("currentUser");
        Template tmpl = ConfigSingleton.getConfig(getServletContext()).getTemplate("account.ftl");
        HashMap<String,Object> map = new HashMap<>();
        map.put("user",user);
        map.put("services",servicesService.findAllToUser(user));
       try {
            tmpl.process(map,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
     /*  response.getWriter().println(user.getId());
       response.getWriter().println(servicesService.findAllToUser(user).toString());*/
    }
}
