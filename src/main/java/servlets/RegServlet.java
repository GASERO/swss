package servlets;

import models.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegServlet extends HttpServlet {

    UserService userService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!userService.check(request.getParameter("regLogin")) && request.getParameter("regPassword").equals(request.getParameter("regPasswordRepeat"))) {
            User newUser = User.builder()
                    .login(request.getParameter("regLogin"))
                    .password(request.getParameter("regPassword"))
                    .firstName(request.getParameter("regFirstname"))
                    .secondName(request.getParameter("regSecondname"))

                    .build();
            userService.save(newUser);
            request.getSession().setAttribute("currentUser",newUser );
            response.sendRedirect("/account");
        }
        else{
            response.getWriter().println("ой, что-то пошло не так" + userService.check(request.getParameter("regLogin"))+request.getParameter("regPassword").equals(request.getParameter("regPasswordRepeat")));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/account");
    }
}
