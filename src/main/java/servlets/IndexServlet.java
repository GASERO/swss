package servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import helpers.ConfigSingleton;
import models.User;
import services.ServicesService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

public class IndexServlet extends HttpServlet {
    UserService userService;
    ServicesService servicesService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        servicesService = new ServicesService();
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user =(User) request.getSession().getAttribute("currentUser");
        Template tmpl = ConfigSingleton.getConfig(getServletContext()).getTemplate("index.ftl");
        HashMap<String,Object> map = new HashMap<>();
        map.put("user",user);
        try {
            tmpl.process(map,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
