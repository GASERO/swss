package servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import helpers.ConfigSingleton;
import models.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


public class LoginServlet extends HttpServlet {
    UserService userService;
    Boolean flag;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        flag = false;
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        boolean result = userService.check(User.builder()
                .login(login)
                .password(request.getParameter("password"))
                .build());
        if (result) {
            if (request.getParameter("remember") != null) {
                 Cookie cookie = new Cookie("login",login);
                 response.addCookie(cookie);
            }
            request.getSession().setAttribute("currentUser",userService.findByLogin(login));
            response.sendRedirect("/account");
        } else {
            flag = true;
            response.sendRedirect("/login");
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession().getAttribute("currentUser")!=null){
            response.sendRedirect("/account");
        }
       Cookie[] cookies = request.getCookies();
        for (Cookie x :
                cookies) {
            if ("login".equals(x.getName())) {
               if(userService.check(x.getName())){
                   response.sendRedirect("/account");
               }
            }
        }

        Template tmpl = ConfigSingleton.getConfig(getServletContext()).getTemplate("login.ftl");
        HashMap<String,Boolean> map = new HashMap<>();
        map.put("flag",flag);
        try {
            tmpl.process(map,response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
