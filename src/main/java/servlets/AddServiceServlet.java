package servlets;

import models.Service;
import models.User;
import services.ServicesService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddServiceServlet extends HttpServlet {
    UserService userService;
    ServicesService servicesService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        servicesService = new ServicesService();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pathInfo = request.getPathInfo(); // /service/value
        String[] pathParts = pathInfo.split("/");
        String command = pathParts[1];
        if (command.equals("add")){
            servicesService.save( Service.builder()
                    .name(request.getParameter("name"))
                    .description(request.getParameter("description"))
                    .price(Integer.parseInt(request.getParameter("price")))
                    .userId(((User)request.getSession().getAttribute("currentUser")).getId())
                    .build());

        } else{
            if (command.equals("delete")){
                servicesService.delete(Long.parseLong(pathParts[2]));
            }
        }
        response.sendRedirect("/account");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println(324);
    }
}
