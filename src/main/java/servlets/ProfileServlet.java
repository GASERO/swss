package servlets;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import helpers.ConfigSingleton;
import models.Service;
import models.User;
import services.ServicesService;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ProfileServlet extends HttpServlet {
    UserService userService;
    ServicesService servicesService;

    @Override
    public void init() throws ServletException {
        userService = new UserService();
        servicesService = new ServicesService();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String pathInfo = request.getPathInfo(); // /profile/id
        String[] pathParts = pathInfo.split("/");
        String id = pathParts[1];
        System.out.println(id);
        User person = userService.findById(Long.parseLong(id));
        System.out.println(person == null);
        if  (person!= null){
            Template tmpl = ConfigSingleton.getConfig(getServletContext()).getTemplate("profile.ftl");
            HashMap<String,Object> map = new HashMap<>();
            ArrayList<Service> services = servicesService.findAllToUser(person);
            User user =(User) request.getSession().getAttribute("currentUser");
            map.put("user",user);
            map.put("person",person);
            map.put("services",services);
            try {
                tmpl.process(map,response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }else{
            response.sendRedirect("/main");
        }
       /* if (command.equals("add")){
            servicesService.save( Service.builder()
                    .name(request.getParameter("name"))
                    .description(request.getParameter("description"))
                    .price(Integer.parseInt(request.getParameter("price")))
                    .userId(((User)request.getSession().getAttribute("currentUser")).getId())
                    .build());

        } else{
            if (command.equals("delete")){
                servicesService.delete(Long.parseLong(pathParts[2]));
            }
        }
        response.sendRedirect("/account");*/

    }
}
