package servlets;

import models.User;
import services.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ChangeLoginAndPasswordServlet extends HttpServlet {
    private UserService userService;
    @Override
    public void init() throws ServletException {
        userService = new UserService();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User)request.getSession().getAttribute("currentUser");
        if (
                (userService.check(request.getParameter("login")) ||
                currentUser.getLogin().equals(request.getParameter("login")))
                && currentUser.getPassword().equals((request.getParameter("password1")))
                ){
            currentUser.setPassword(request.getParameter("password2"));
            userService.updateLoginAndPassword(currentUser);
            response.getWriter().println(true);
        }else{
                    response.getWriter().println(false);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/account");
    }
}
