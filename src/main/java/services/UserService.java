package services;

import dao.UserDao;
import daoImpl.UserDaoJDBCImpl;
import models.User;

public class UserService {
    private UserDao userDao;

    public UserService() {
        this.userDao = new UserDaoJDBCImpl();
    }

    public boolean check(User user){
        return userDao.findByLoginAndPassword(user)!= null;
    }
    public boolean check(String login){
        return userDao.findByLogin(login) != null;
    }

    public User findByLogin(String login) {
        return userDao.findByLogin(login);
    }
    public void save(User user){
        userDao.save(user);
    }

    public void update(User user) {
        userDao.update(user.getId(),user);
    }
    public void updateLoginAndPassword(User user) {
        userDao.updateLoginAndPassword(user.getId(),user);
    }

    public User findById(Long id) {
      return   userDao.find(id);
    }
}
