package services;

import dao.ServiceDao;
import daoImpl.ServiceDaoJDBCImpl;
import models.Service;
import models.User;

import java.util.ArrayList;

public class ServicesService {
    private ServiceDao serviceDao;

    public ServicesService() {
        this.serviceDao = new ServiceDaoJDBCImpl();
    }
    public ArrayList<Service> findAllToUser(User user){
      return serviceDao.findAllByUserId(user.getId());
    }
    public void save(Service service){
        serviceDao.save(service);
    }

    public void delete(long l) {
        serviceDao.delete(l);
    }
}
