package dao;

import models.Service;

import java.util.ArrayList;


public interface ServiceDao extends CrudDao<Service,Long>{
    ArrayList<Service> findAllByUserId(Long id);
}
