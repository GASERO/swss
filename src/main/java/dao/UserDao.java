package dao;

import models.User;

public interface UserDao extends CrudDao<User,Long>{
    public User findByLoginAndPassword(User model);
    public User findByLogin(String login);
    public void updateLoginAndPassword(Long n,User model);
}




