package dao;

import models.Order;

public interface OrderDao extends CrudDao<Order,Long>{
}
