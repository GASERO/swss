package daoImpl;


import dao.UserDao;
import models.User;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class UserDaoJDBCImpl implements UserDao {


    //language=SQL
    private final static String SQL_INSERT_USER = "INSERT INTO \"user\" (user_name,password,firstname,secondname) VALUES (?,?,?,?)";
    private static final String SQL_FIND_ALL = "SELECT * FROM student";
    private static final String SQL_FIND_USER_BY_PL = "SELECT * FROM \"user\" where user_name = ? and password = ?";
    private static final String SQL_FIND_USER_BY_L = "SELECT * FROM \"user\" where user_name = ?";
    private static final String SQL_DELETE_STUDENT = "DELETE FROM student WHERE student.id = ?";
    private static final String SQL_FIND_USER_BY_ID = "SELECT * FROM \"user\" WHERE id = ?";
    private static final String SQL_UPDATE_USERS_PL = "update \"user\" set user_name =?, password = ? where id = ?";
    private static final String SQL_UPDATE_USER= "UPDATE \"user\" SET firstname = ? ,secondname=?, phone = ?, description = ?, profession = ?, college = ?,date_finish = ? where id=?";


    private Connection connection;

    public UserDaoJDBCImpl() {
        this.connection = helpers.ConnectionSingleton.getConnection();
    }

    @Override
    @Deprecated
    public void save(User model) {
        try {
            PreparedStatement statement =
                    connection.prepareStatement(SQL_INSERT_USER,new String[] {"id"});
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPassword());
            statement.setString(3, model.getFirstName());
            statement.setString(4, model.getSecondName());
            statement.executeUpdate();
            // получаем указатель на результирующие строки
            // результирующие строки - сгенерированный id
            ResultSet resultSet = statement.getGeneratedKeys();
            // одновременно сдвигаем итератор и проверяем есть там че или нет
            if (resultSet.next()) {
                long id = resultSet.getLong(1);
                model.setId(id);
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Deprecated
    @Override
    public User find(Long id) {
       PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_USER_BY_ID);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return  User.builder()
                        .id(resultSet.getLong("id"))
                        .firstName(resultSet.getString("firstname"))
                        .secondName(resultSet.getString("secondname"))
                        .sex(resultSet.getString("sex"))
                        .avatarUrl(resultSet.getString("avatar_url"))
                        .description(resultSet.getString("description"))
                        .cityId(resultSet.getString("city_id"))
                        .phone(resultSet.getString("phone")+"")
                        .profession(resultSet.getString("profession"))
                        .college(resultSet.getString("college"))
                        .dateFinish(resultSet.getInt("date_finish"))
                        .build();
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public User findByLoginAndPassword(User model) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_USER_BY_PL);
            statement.setString(1, model.getLogin());
            statement.setString(2, model.getPassword());


            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return  User.builder()
                        .id(resultSet.getLong("id"))
                        .login(resultSet.getString("user_name"))
                        .password(resultSet.getString("password"))
                        .firstName(resultSet.getString("firstname"))
                        .secondName(resultSet.getString("secondname"))
                        .sex(resultSet.getString("sex"))
                        .avatarUrl(resultSet.getString("avatar_url"))
                        .description(resultSet.getString("description"))
                        .cityId(resultSet.getString("city_id"))
                        .phone(resultSet.getString("phone")+"")
                        .profession(resultSet.getString("profession"))
                        .college(resultSet.getString("college"))
                        .dateFinish(resultSet.getInt("date_finish"))
                        .build();
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public User findByLogin(String login){
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_FIND_USER_BY_L);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return  User.builder()
                        .id(resultSet.getLong("id"))
                        .login(resultSet.getString("user_name"))
                        .password(resultSet.getString("password"))
                        .firstName(resultSet.getString("firstname"))
                        .secondName(resultSet.getString("secondname"))
                        .sex(resultSet.getString("sex"))
                        .avatarUrl(resultSet.getString("avatar_url"))
                        .description(resultSet.getString("description"))
                        .cityId(resultSet.getString("city_id"))
                        .phone(resultSet.getString("phone"))
                        .profession(resultSet.getString("profession"))
                        .college(resultSet.getString("college"))
                        .dateFinish(resultSet.getInt("date_finish"))
                        .build();
            } else return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }



    }

    @Override
    public ArrayList<User> findAll() {
       /* PreparedStatement statement = null;
        ArrayList<User> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(new Human.Builder()
                        .age(resultSet.getInt("age"))
                        .color(resultSet.getString("color"))
                        .name(resultSet.getString("name"))
                        .id(resultSet.getLong("id"))
                        .build());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;*/
       return null;
    }

    @Override
    public void delete(Long id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_STUDENT);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(Long n, User model) {
        PreparedStatement statement;
        try {
            statement = connection.prepareStatement(SQL_UPDATE_USER);
            statement.setString(1,model.getFirstName());
            statement.setString(2,model.getSecondName());
            statement.setString(3,model.getPhone());
            statement.setString(4,model.getDescription());
            statement.setString(5,model.getProfession());
            statement.setString(6,model.getCollege());
            statement.setInt(7,model.getDateFinish());
            statement.setLong(8, n);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    @Override
    public void updateLoginAndPassword(Long n, User model){
        PreparedStatement statement;
        try{
            statement = connection.prepareStatement(SQL_UPDATE_USERS_PL);
            statement.setString(1,model.getLogin());
            statement.setString(2,model.getPassword());
            statement.setLong(3,n);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
