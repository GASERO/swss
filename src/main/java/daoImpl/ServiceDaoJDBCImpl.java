package daoImpl;


import dao.ServiceDao;
import dao.UserDao;
import models.Service;
import models.User;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class ServiceDaoJDBCImpl implements ServiceDao {


    //language=SQL
    private final static String SQL_INSERT_SERVICE = "INSERT INTO service (name,description,photo,price,user_id) VALUES (?,?,?,?,?)";
    private static final String SQL_FIND_ALL = "SELECT * FROM student";
   // private static final String SQL_FIND_USER_BY_PL = "SELECT * FROM \"user\" where user_name = ? and password = ?";
    private static final String SQL_FIND_ALL_SERVICES_BY_USERID = "SELECT * FROM service where user_id = ?";
    private static final String SQL_DELETE_SERVICE = "DELETE FROM service WHERE service.id = ?";
    private static final String SQL_FIND_OWNER = "SELECT * FROM student WHERE service.id = ?";


    private Connection connection;

    public ServiceDaoJDBCImpl() {
        this.connection = helpers.ConnectionSingleton.getConnection();
    }

    @Override
    @Deprecated
    public void save(Service model) {
        try {
            PreparedStatement statement =
                    connection.prepareStatement(SQL_INSERT_SERVICE);
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setString(3, model.getPhoto());
            statement.setInt(4, model.getPrice());
            statement.setLong(5,model.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Deprecated
    @Override
    public Service find(Long id) {
        PreparedStatement statement = null;
  /*      try {
            statement = connection.prepareStatement(SQL_FIND_OWNER);
            statement.setLong(1, id);


            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return new Human.Builder()
                        .age(resultSet.getInt("age"))
                        .color(resultSet.getString("color"))
                        .name(resultSet.getString("name"))
                        .id(resultSet.getLong("id"))
                        .build();
            } else throw new IllegalArgumentException("user not found");
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }*/
        return null;
    }

    @Override
    public ArrayList<Service> findAllByUserId(Long id){

        PreparedStatement statement = null;
        ArrayList<Service> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL_SERVICES_BY_USERID);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add( Service.builder()
                        .id(resultSet.getLong("id"))
                        .description(resultSet.getString("description"))
                        .photo(resultSet.getString("photo"))
                        .price(resultSet.getInt("price"))
                        .name(resultSet.getString("name"))
                        .userId(id)
                        .build());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;




    }

    @Override
    public ArrayList<Service> findAll() {
       /* PreparedStatement statement = null;
        ArrayList<User> list = new ArrayList<>();
        try {
            statement = connection.prepareStatement(SQL_FIND_ALL);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                list.add(new Human.Builder()
                        .age(resultSet.getInt("age"))
                        .color(resultSet.getString("color"))
                        .name(resultSet.getString("name"))
                        .id(resultSet.getLong("id"))
                        .build());
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;*/
        return null;
    }

    @Override
    public void delete(Long id) {
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(SQL_DELETE_SERVICE);
            statement.setLong(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void update(Long n, Service model) {

    }
}
