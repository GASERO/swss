package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Order {
    private String name;
    private String phone;
    private String description;
    private Long userId;
    private Long specialist;
    private Long status;
    private Long serviceId;
    private Long serviceTypeId;
}
