package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class User {
    private String login;
    private Long id;
    private String password;
    private String firstName;
    private String secondName;
    private String sex;
    private String avatarUrl;
    private String description;
    private String cityId;
    private String phone;
    private String profession;
    private String college;
    private Integer dateFinish;
}
