package models;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Builder
public class Service {
    Long id;
    String description;
    String photo;
    Integer price;
    String name;
    Long userId;
}
