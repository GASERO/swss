<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
</head>
<body>

<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand">
                    <img src="../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Выберите город
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" name="сity">Казань</a>
                    <a class="dropdown-item" name="сity">Набережные Челны</a>
                    <a class="dropdown-item" name="сity">Альметьевск</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/main">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Скидки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Карта сайта</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О сайте</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/account">${user.firstName}</a>
            </li>
        <#--  <li class="nav-item">
              <a class="nav-link" href="/login">Войти</a>
          </li>-->
            <li class="nav-item">
                <a class="nav-link" role="button" name="logout" href="/logout">Выйти</a>
            </li>
        </ul>

    </div>
</nav>

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Профиль</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#orders" role="tab">Мои заказы</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#feedback" role="tab">Мои отзывы</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#service" role="tab">Мои услуги</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="profile" role="tabpanel">
        <div class="container">
		   <!-- добавить в форму enctype="multipart/form-data" -->
            <form action="" method="post" class="form-control-lg">
                <div class="row">
                    <div class="form-group col-lg-8">
                        <h2>Основная информация</h2>

                        <p><input type="text" pattern="[А-Яа-яёA-za-z]+" name="firstName" placeholder="Имя" value="${user.firstName}" required autofocus></p>
                        <p><input type="text" pattern="[А-Яа-яёA-za-z]+" name="secondName" placeholder="Фамилия" value="${user.secondName}" required></p>
                        <p><input type="text" pattern="^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$" 
						name="phone" placeholder="Телефон"  value="<#if user.phone??> ${user.phone}</#if>" required></p>
                        <label>
                            <select name="city" style="width:115%;">
                                <option>Казань</option>
                                <option>Набережные Челны</option>
                                <option>Альметьевск</option>
                            </select>
                        </label>
                        <h5>Добавьте аватар:</h5>
                        <p><input type="file" name="avatar"></p>

                        <h5>Информация о себе:</h5>
                        <textarea class="form-control" type="text" name="description" rows="4" required
                                  placeholder=""><#if user.description??> ${user.description}</#if></textarea>
                    </div>
                    <div class="col-lg-4">
                        <h2>Образование</h2>

                        <p><input type="text" pattern="[А-Яа-яёA-za-z\s]+" name="job" placeholder="Специальность" value="<#if user.profession??> ${user.profession}</#if>" required></p>
                        <p><input type="text" pattern="[А-Яа-яёA-za-z\s]+" name="institute" placeholder="Учебное заведение" value="<#if user.college??> ${user.college}</#if>" required>
                        </p>
                        <p><input type="text" <#--pattern="[1-2][0-9]{3}"--> name="year" placeholder="Год завершения обучения" value="<#if user.dateFinish !=0> ${user.dateFinish}</#if>" required>
                        </p>
                    </div>
                </div>

                <form method="post">
                <button class="btn btn-lg btn-primary" type="submit" >Сохранить</button>
                </form>
                <button class="btn btn-lg btn-primary" type="button" data-toggle="modal" data-target="#myModal">
                    Сменить логин или пароль
                </button>
            </form>
        </div>
    </div>


    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Смена логина или пароля</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form class="form-signin">
                            <p><input type="text" class="form-control" name="new_login" placeholder="Новый логин" id="newLogin"
                                      value="${user.login}" required autofocus oninput="checkLogin()"></p>

                            <p><input type="password" class="form-control" name="old_password" id="oldPassword"
                                      placeholder="Старый пароль" required></p>
                            <p><input type="password" class="form-control" name="new_password" id="newPassword"
                                      placeholder="Новый пароль" required></p>
                        </form>
                            <button onclick="tryToChange()" id="submitChange" class="btn btn-lg btn-primary btn-block" >Сохранить</button>

                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


    <div class="tab-pane" id="orders" role="tabpanel">
            <div class="container space" style=" min-height: calc(100vh - 329px);">
            <div class="row">
                <div class="well col-sm-2">
                    <h5 class="text-center">Статус</h5>
                    <div class="radio">
                        <label><input type="radio" checked="checked" name="optradio">Активные</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="optradio">Завершённые</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="optradio">Отклонённые</label>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="well">
                        <h2>Lorem ipsum dolor sit amet</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                            recusandae tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit.
                            Deleniti facere reiciendis voluptate!</p>
                        <p class="text-right" style="margin: 0">
                            <button class="btn btn-primary" href="#" role="button">Подробнее</button>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ReviewModal"
                                    role="button">Оставить
                                отзыв
                            </button>
                        </p>
                    </div>
                    <div class="well">
                        <h2>Lorem ipsum dolor sit amet</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                            recusandae tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit.
                            Deleniti facere reiciendis voluptate!</p>
                        <p class="text-right" style="margin: 0">
                            <button class="btn btn-primary" href="#" role="button">Подробнее</button>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ReviewModal"
                                    role="button">Оставить
                                отзыв
                            </button>
                        </p>
                    </div>
                    <div class="well">
                        <h2>Lorem ipsum dolor sit amet</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                            recusandae tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit.
                            Deleniti facere reiciendis voluptate!</p>
                        <p class="text-right" style="margin: 0">
                            <button class="btn btn-primary" href="#" role="button">Подробнее</button>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#ReviewModal"
                                    role="button">Оставить
                                отзыв
                            </button>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div id="ReviewModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Создание отзыва</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form action="" method="post" class="form-control-lg">
							<p class="text-center">Напишите отзыв длинной не более 300 символов</p>
                            <textarea type="text" class="form-control" name="review" id="review" 
							oninput="checkLength()" rows="10" placeholder="Ваш отзыв"
                                      required autofocus></textarea>
                            <button class="btn btn-lg btn-primary btn-block" id ="addReviewBtn" type="submit">Отправить</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


    <div class="tab-pane" id="feedback" role="tabpanel">
           <div class="container space" style=" min-height: calc(100vh - 329px);">
            <div class="well">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                    recusandae
                    tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti facere
                    reiciendis voluptate!</p>
                <p class="text-right" style="margin: 0">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#EditModal" role="button">
                        Редактировать
                        отзыв
                    </button>
                </p>
            </div>
            <div class="well">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                    recusandae
                    tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti facere
                    reiciendis voluptate!</p>
                <p class="text-right" style="margin: 0">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#EditModal" role="button">
                        Редактировать
                        отзыв
                    </button>
                </p>
            </div>
            <div class="well">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                    recusandae
                    tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti facere
                    reiciendis voluptate!</p>
                <p class="text-right" style="margin: 0">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#EditModal" role="button">
                        Редактировать
                        отзыв
                    </button>
                </p>
            </div>
            <div class="well">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia placeat
                    recusandae
                    tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti facere
                    reiciendis voluptate!</p>
                <p class="text-right" style="margin: 0">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#EditModal" role="button">
                        Редактировать
                        отзыв
                    </button>
                </p>
            </div>
        </div>
    </div>

    <div id="EditModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Редактирование отзыва</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <form action="" method="post" class="form-control-lg">
							<p class="text-center">Напишите отзыв длинной не более 300 символов</p>
                            <textarea type="text" class="form-control" id="editReview" oninput="checkLength()" name="review" rows="10" placeholder="Ваш отзыв"
                                      required autofocus>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci distinctio exercitationem explicabo maxime minus nihil sequi sint tempora voluptates? Asperiores autem consequatur ex maxime quidem quisquam ullam. Consectetur dolorem, quasi!
                            </textarea>
                            <button class="btn btn-lg btn-primary btn-block" id="editReviewBtn" type="submit">Cохранить</button>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
        </div>
    </div>


    <div class="tab-pane" id="service" role="tabpanel">
         <div class="container marketing" style=" min-height: calc(100vh - 259px);">
            <div class="text-right" style="margin: 10px 10px">
                <button class="btn btn-primary" data-toggle="modal" data-target="#AddWindow-id" role="button"> Добавить
                </button>
            </div>
            <!-- Three columns of text below the carousel -->
            <div class="row">

                <script type="text/javascript">
                    function f(name,desc,price) {
                        $("#EditWindow-id").modal('show');
                      //  $("#EditWindow-id").getElementById("e_name").setAttribute("value",data);
                        $("#e_name").val(name);
                        $("#e_desc").val(desc);
                        $("#e_price").val(price);

                    }
                </script>




            <#if services?has_content>
                <#list services as s>
                    <div>
                        <h2>${s.name}</h2>
                        <img src="../static/img/portfolio_08.jpg" width="280" height="280">
                        <p>${s.description}</p>
                        <p>
                           <#-- <button class="btn btn-primary" onclick="f( &quot ${s.name} &quot,&quot ${s.description}  &quot,&quot ${s.price} &quot)"-->
                            <button class="btn btn-primary" onclick="f('${s.name}','${s.description}','${s.price}')"
                                    type="button" data-toggle="modal" <#--data-target="#EditWindow-id" -->>
                                Изменить
                            </button>

                        <form method="post" action="service/delete/${s.id}">
                            <button class="btn btn-primary" role="button">Удалить
                            </button>
                        </form>
                        </p>
                    </div>
                </#list>
            <#else>
                Нет услугов(
            </#if>
                <#--<div class="well col-lg-4 ">
                    <h2>Услуга 1</h2>
                    <img src="../static/img/portfolio_08.jpg" width="280" height="280">
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#EditWindow-id">
                            Изменить
                        </button>
                        <button formmethod="post" class="btn btn-primary" role="button">Удалить
                        </button>
                    </p>

                </div><!-- /.col-lg-4 &ndash;&gt;
                <div class="well col-lg-4 ">
                    <h2>Услуга 2</h2>
                    <img src="../static/img/portfolio_09.jpg" width="280" height="280">
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#EditWindow-id">
                            Изменить
                        </button>
                        <button formmethod="post" class="btn btn-primary" role="button">Удалить</button></p>
                </div><!-- /.col-lg-4 &ndash;&gt;
                <div class="well col-lg-4 ">
                    <h2>Услуга 3</h2>
                    <img src="../static/img/portfolio_10.jpg" width="280" height="280">
                    <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p>
                        <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#EditWindow-id">
                            Изменить
                        </button>
                        <button formmethod="post" class="btn btn-primary" href="#" role="button">Удалить</button></p>
                </div>-->


                    <!-- /.col-lg-4 -->

                <!--<div style="margin: auto"><a class="btn btn-primary" href="#" role="button">Показать ещё</a></div>-->
            </div><!-- /.row -->
        </div>
    </div>
</div>


<div id="EditWindow-id" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Редактирование услуги</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="" method="post" class="form-signin">

                        <label for="type" class="sr-only">Type</label>
                        <input type="text" id="e_name" class="form-control" name="e_name" placeholder="Название"
                               required autofocus >
                        <label for="info" class="sr-only">Info</label>
                        <textarea type="text" id="e_desc" class="form-control" name="e_type"
                                  placeholder="Краткая информация" rows="10" required></textarea>
                        <label for="experience" class="sr-only">Experience</label>
                        <input type="text" id="experience" class="form-control" name="e_experience"
                               placeholder="Опыт работы" required>
                        <label for="examples" class="sr-only">Examples</label>
                        <p>Примеры работ<input type="file" id="examples" class="form-control" name="e_example" required>
                        </p>
                        <label for="price" class="sr-only">Price</label>
                        <textarea type="text" id="e_price" class="form-control" name="e_price"
                                  placeholder="Цены подуслуг" rows="10" required></textarea>
                        <!--    <p>Дата размещения<input type="date" class="form-control" name="e_date"></p>


                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                lorem ipsum
                            </div>
                            -->
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Cохранить</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div id="AddWindow-id" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавление услуги</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="service/add" method="post" class="form-signin">

                        <label for="type" class="sr-only">Type</label>
                        <input type="text" id="type2" class="form-control" name="name" placeholder="Название"
                               required autofocus>
                        <label for="info" class="sr-only">Info</label>
                        <textarea type="text" id="info2" class="form-control" name="description"
                                  placeholder="Краткая информация" rows="10" required></textarea>
                        <label for="experience" class="sr-only">Experience</label>
                        <input type="text" id="experience2" class="form-control" name="experience"
                               placeholder="Опыт работы" required>
                        <label for="examples" class="sr-only">Examples</label>
                        <p>Примеры работ<input type="file" id="examples2" class="form-control" name="example" required>
                        </p>
                        <label for="price" class="sr-only">Price</label>
                        <textarea type="text" id="price2" class="form-control" name="price"
                                  placeholder="Цены подуслуг" rows="10" required></textarea>
                        <!--    <p>Дата размещения<input type="date" class="form-control" name="date"></p>


                            <div class="alert alert-warning alert-dismissible fade s    how" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                lorem ipsum
                            </div>
                            -->
                        <button class="btn btn-lg btn-primary btn-block" type="submit" >Cохранить</button>

                       <#-- <a href="/service/add">se,fls,glr,gld</a>-->
                        </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<@footer/>


<!-- jQuery first, then Tether, then Bootstrap JS. &ndash;&gt;
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="../static/js/bootstrap.min.js"></script>

<script type="application/javascript">
    function checkLength() {
        var text = $("#review").val();
        var btn = document.getElementById("addReviewBtn");
        btn.disabled = text.length > 300;

        var text2 = $("#editReview").val();
        var btn2 = document.getElementById("editReviewBtn");
        btn2.disabled = text2.length > 300;
    }
    function tryToChange(){
        $.ajax({
            url: '/changeLP',
            type: 'POST',
            data:{
                login:   $('#newLogin').val(),
                password1: $('#oldPassword').val(),
                password2: $('#newPassword').val()
            },
            dataType: 'json',
            success: function (data) {

                if (data == true){
                    console.log("OK");

                }else{
                    console.log("NE OK")
                }


            },
            error: function () {
                console.log("eror")
            }
        });
    }
    function checkLogin(){
        $.ajax({
            url: '/check',
            type: 'POST',
            data:{
                login:   $('#newLogin').val()},
            dataType: 'json',
            success: function (data) {

                if (data == false){
                    console.log("login is free");

                }else{
                    console.log("login is reserved")
                }
                if (!data ||  $('#newLogin').val() == '${user.login}') {
                    console.log(1);
                    $('#submitChange').attr('disabled', false);
                }else{
                    $('#submitChange').attr('disabled', true);
                    console.log(2);
                }

            },
            error: function () {
                console.log("Login is reserved")
            }
        });
    }
</script>
</body>
</html>