<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
</head>
<body>

<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand" href="index.html">
                    <img src="../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="inp_impact" name="city" data-toggle="dropdown">
                    <span id="text">Выберите город</span></a>
                <ul ID="divNewNotifications" class="dropdown-menu">
                    <li><a class="dropdown-item">Казань</a></li>
                    <li><a class="dropdown-item">Набережные Челны</a></li>
                    <li><a class="dropdown-item">Альметьевск</a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.html">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="profile.html">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="order.html">Скидки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Карта сайта</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О сайте</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            </li>
        <#if user??>
            <li class="nav-item">
                <a tabindex="0" class="nav-link" data-toggle="tooltip" title="Войти в личный кабинет" href="/account">${user.firstName}</a>
            </li>

            <li class="nav-item">
                <form method="post" action="logout">
                    <a href="/logout" class="nav-link" role="button" name="logout">Выйти</a>

                </form>
            </li>
        <#else>


            <li class="nav-item">
                <a class="nav-link" href="/login">Войти</a>
            </li>
        </#if>
        </ul>

    </div>
</nav>

<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading"><strong>Выбирайте только самых проверенных специалистов на
            http://localhost:8080/</strong></h1>

        <form action="/search" method="GET" class="form-inline" style="justify-content: center;">

            <input type="text" class="form-control form-control-lg" placeholder="Введите услугу или специальность"
                   style="width: 80%"/>
            <button type="submit" name="sr" class="btn btn-primary btn-lg">Найти</button>
        </form>

    </div>
</section>


<div class="container space"><h3 class="text-center">Топ специалистов за последний месяц</h3></div>
<div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
        <div class="col-lg-4">
            <img class="rounded-circle" src="../static/img/portfolio_01.jpg" alt="Generic placeholder image"
                 width="140"
                 height="140">
            <h2>Иванов Иван Иванович</h2>
            <h6>На все руки мастер</h6>
            <h6>Рейтинг: 10</h6>
            <h6>10 отзывов: 10</h6>
            <h6>Средняя оценка 10: 10</h6>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo
                cursus magna.</p>
            <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="rounded-circle" src="../static/img/portfolio_02.jpg" alt="Generic placeholder image"
                 width="140"
                 height="140">
            <h2>Иванов Иван Иванович</h2>
            <h6>На все руки мастер</h6>
            <h6>Рейтинг: 10</h6>
            <h6>10 отзывов: 10</h6>
            <h6>Средняя оценка 10: 10</h6>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo
                cursus magna.</p>
            <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <img class="rounded-circle" src="../static/img/portfolio_03.jpg" alt="Generic placeholder image"
                 width="140"
                 height="140">
            <h2>Иванов Иван Иванович</h2>
            <h6>На все руки мастер</h6>
            <h6>Рейтинг: 10</h6>
            <h6>10 отзывов: 10</h6>
            <h6>Средняя оценка 10: 10</h6>
            <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
                vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo
                cursus magna.</p>
            <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
        </div><!-- /.col-lg-4 -->
        <div style="margin: auto">
            <button type="button" class="btn btn-primary" id="showMore" href="#" role="button">Показать ещё</button>
        </div>
    </div><!-- /.row -->
</div>

<div class="container space"><h3 class="text-center">Самые популярные услуги за последний месяц</h3></div>
<section class="carucel">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide"
                     src="../static/img/portfolio_04.jpg"
                     alt="First slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block">
                        <h1>Another example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide"
                     src="../static/img/portfolio_05.jpg"
                     alt="Second slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block">
                        <h1>Another example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide"
                     src="../static/img/portfolio_07.jpg"
                     alt="Third slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block">
                        <h1>Another example headline.</h1>
                        <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta
                            gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>

<@footer/>
<@scripts/>
</body>
</html>