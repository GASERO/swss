<!DOCTYPE html>
<html lang="en">
<head>
    <#macro head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../static/css/bootstrap.min.css">
    <link rel="stylesheet" href="../static/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="../static/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    </#macro>
    -->
</head>

<body>

<#macro nav>
<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand" href="index.html">
                    <img src="../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="inp_impact" name="city" data-toggle="dropdown">
                    <span id="text">Выберите город</span></a>
                <ul ID="divNewNotifications" class="dropdown-menu">
                    <li><a class="dropdown-item">Казань</a></li>
                    <li><a class="dropdown-item">Набережные Челны</a></li>
                    <li><a class="dropdown-item">Альметьевск</a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.html">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            </li>
            <li class="nav-item">
                <a tabindex="0" class="nav-link" data-toggle="tooltip" title="Войти в личный кабинет" href="account.html">Иван</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="auth.html">Войти</a>
            </li>
            <li class="nav-item">
                <form method="post">
                <a class="nav-link" role="submit" name="logout">Выйти</a>
                </form>
            </li>
        </ul>

    </div>
</nav>

<div style=" min-height: calc(100vh - 207px);"></div>
</#macro>


<#macro footer>

<footer id="footerwrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                <h4>Обратная связь</h4>
                <div class="hline-w"></div>
                <p>
                    <a href="https://vk.com/kazan_federal_university"><i class="fa fa-vk" aria-hidden="true"></i></a>
                    <a href="https://www.facebook.com/KazanUniversity/"><i class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/kazanuniversity"><i class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/kazanfederaluniversity/"><i class="fa fa-instagram"></i></a>
                    <a href="https://www.youtube.com/univertv"><i class="fa fa-youtube"></i></a>
                </p>
                <p>&copy; 2017</p>
            </div>

            <div class="col-lg-3">
                <h4>Наш проект</h4>
                <div class="hline-w"></div>
                <ul class="list-unstyled">
                    <li><a href="">Планы</a></li>
                    <li><a href="">Обратная связь</a></li>
                    <li><a href="">FAQ</a></li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h4>Команда</h4>
                <div class="hline-w"></div>
                <ul class="list-unstyled">
                    <li><a href="">Разработчики</a></li>
                    <li><a href="">Инвесторы</a></li>
                    <li><a href="">Партнёры</a></li>
                </ul>
            </div>
        </div>
        <! --/row -->
    </div>
    <! --/container -->
</footer>
<! --/footerwrap -->


</#macro>

<#macro scripts>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="../static/js/bootstrap.min.js"></script>
<script>
    $('.dropdown-toggle').dropdown();
    $('#divNewNotifications li > a').click(function () {
        $('#text').text($(this).html());
    });
</script>


</#macro>

</body>
</html>