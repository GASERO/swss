<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
</head>
<body>


<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand">
                    <img src="../../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Выберите город
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" name="сity">Казань</a>
                    <a class="dropdown-item" name="сity">Набережные Челны</a>
                    <a class="dropdown-item" name="сity">Альметьевск</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../pages/index.html">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Скидки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Карта сайта</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О сайте</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../pages/account.html">Иван</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../../pages/login.html">Войти</a>
            </li>
            <li class="nav-item">
                <form method="post">
                    <a class="nav-link" role="button" name="logout">Выйти</a>
                </form>
            </li>
        </ul>

    </div>
</nav>


<div class="container-fluid space">
    <form action="" class="form-inline" style="justify-content: center;">

        <input type="text" class="form-control form-control-lg" placeholder="Введите услугу или специальность" value="<#if sr??>${sr}</#if>"
               style="width: 90%"/>
        <button type="submit" class="btn btn-primary btn-lg">Найти</button>
    </form>
    <div class="row space">
        <div class="col-lg-2">
            <div class="well">
                <h4 class="text-center">Фильтр</h4>
                <div class="checkbox">
                    <label><input type="checkbox" name="p1" value="C отзывами"> C отзывами</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="p2" value="С гарантией"> С гарантией</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="p5" value="С акциями"> С акциями</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="p6" value="С фото"> С фото</label>
                </div>
                <div style="margin-top: 30px">
                    <h4 class="text-center">Сортировка</h4>
                    <div class="radio">
                        <p><input type="radio" checked name="optradio"> По рейтингу</p>
                        <p><input type="radio" name="optradio"> По отзывам</p>
                        <p><input type="radio" name="optradio"> По цене</p>
                    </div>
                </div>
                <div style="margin-top: 30px" class="text-center">
                    <h4>Актуальные данные о выбранной услуге</h4>
                    <p> 225 отзывов</p>
                    <p> 207 мастеров</p>
                    <p>4,11 средний рейтинг</p>
                </div>
            </div>
        </div>


        <div class="col-lg-7">
            <div class="well">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="../../static/img/portfolio_03.jpg" width="140" height="140">
                            </div>
                            <div class="col-lg-7 text-center">
                                <a href="../../pages/profile.html"><h2>Хакимов Динар Иршатович</h2></a>
                                <p>Мелкий ремонт, сантехника, бытовая техника</p>
                            </div>
                            <div style="margin-left: 20px">
                                <h4>Отзывы</h4>
                                <p> Компетентный и хороший мастер. Свою работу выполнил быстро и качественно</p>
                                <a href="#">Все отзывы (100)</a>
                                <h4>Цены</h4>
                                <p> Монтаж сантехнического оборудования по договорённости</p>
                                <a href="#">Все цены (20)</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-lg btn-primary btn-block">Выбрать</button>

                        <div class="space text-center">
                            <h4>Рейтинг 5++</h4>
                            <h4>138 отзывов</h4>
                        </div>
                        <h6><i class="fa fa-check"></i> Работает по договору</h6>
                        <h6><i class="fa fa-check"></i> Даёт гарантию</h6>
                        <h6><i class="fa fa-check"></i> Собеседование пройдено</h6>
                        <h6><i class="fa fa-check"></i> Данные проверены</h6>
                    </div>
                </div>
            </div>

            <div class="well">

                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="../../static/img/portfolio_03.jpg" width="140" height="140">
                            </div>
                            <div class="col-lg-7 text-center">
                                <a href="#"><h2>Хакимов Динар Иршатович</h2></a>
                                <p>Мелкий ремонт, сантехника, бытовая техника</p>
                            </div>
                            <div style="margin-left: 20px">
                                <h4>Отзывы</h4>
                                <p> Компетентный и хороший мастер. Свою работу выполнил быстро и качественно</p>
                                <a href="#">Все отзывы (100)</a>
                                <h4>Цены</h4>
                                <p> Монтаж сантехнического оборудования по договорённости</p>
                                <a href="#">Все цены (20)</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-lg btn-primary btn-block">Выбрать</button>

                        <div class="space text-center">
                            <h4>Рейтинг 5++</h4>
                            <h4>138 отзывов</h4>
                        </div>
                        <h6><i class="fa fa-check"></i> Работает по договору</h6>
                        <h6><i class="fa fa-check"></i> Даёт гарантию</h6>
                        <h6><i class="fa fa-check"></i> Собеседование пройдено</h6>
                        <h6><i class="fa fa-check"></i> Данные проверены</h6>
                    </div>
                </div>
            </div>

            <div class="well">

                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="../../static/img/portfolio_03.jpg" width="140" height="140">
                            </div>
                            <div class="col-lg-7 text-center">
                                <a href="#"><h2>Хакимов Динар Иршатович</h2></a>
                                <p>Мелкий ремонт, сантехника, бытовая техника</p>
                            </div>
                            <div style="margin-left: 20px">
                                <h4>Отзывы</h4>
                                <p> Компетентный и хороший мастер. Свою работу выполнил быстро и качественно</p>
                                <a href="#">Все отзывы (100)</a>
                                <h4>Цены</h4>
                                <p> Монтаж сантехнического оборудования по договорённости</p>
                                <a href="#">Все цены (20)</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-lg btn-primary btn-block">Выбрать</button>

                        <div class="space text-center">
                            <h4>Рейтинг 5++</h4>
                            <h4>138 отзывов</h4>
                        </div>
                        <h6><i class="fa fa-check"></i> Работает по договору</h6>
                        <h6><i class="fa fa-check"></i> Даёт гарантию</h6>
                        <h6><i class="fa fa-check"></i> Собеседование пройдено</h6>
                        <h6><i class="fa fa-check"></i> Данные проверены</h6>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-lg-3 text-center">
            <div class="well">
                <h3>Другие услуги</h3>
                <div>
                    <hr align="center" width="100%" size="1" color="#fafafa"/>
                    <h3>Услуга 1</h3>
                    <img src="../../static/img/portfolio_08.jpg" width="140" height="140">
                    <p style="margin-top: 10px">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
                </div>
                <div>
                    <hr align="center" width="100%" size="1" color="#fafafa"/>
                    <h3>Услуга 1</h3>
                    <img src="../../static/img/portfolio_08.jpg" width="140" height="140">
                    <p style="margin-top: 10px">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
                </div>
                <div>
                    <hr align="center" width="100%" size="1" color="#fafafa"/>
                    <h3>Услуга 1</h3>
                    <img src="../../static/img/portfolio_08.jpg" width="140" height="140">
                    <p style="margin-top: 10px">Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh
                        ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
                    <p><a class="btn btn-primary" href="#" role="button">Подробнее</a></p>
                </div>
            </div>
        </div><!-- /.col-lg-4 -->
    </div>
</div>


<@footer/>


<@scripts/>

</body>
</html>