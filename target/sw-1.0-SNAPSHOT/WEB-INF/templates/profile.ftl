<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <link rel="stylesheet" href="../static/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="../static/owlcarousel/owl.theme.green.min.css">
    <link rel="stylesheet" href="../static/owlcarousel/owl.carousel.min.css">
</head>
<body>

<!--
<#--     <@nav/>-->
    -->
<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand" href="index.html">
                    <img src="../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="inp_impact" name="city" data-toggle="dropdown">
                    <span id="text">Выберите город</span></a>
                <ul ID="divNewNotifications" class="dropdown-menu">
                    <li><a class="dropdown-item">Казань</a></li>
                    <li><a class="dropdown-item">Набережные Челны</a></li>
                    <li><a class="dropdown-item">Альметьевск</a></li>
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.html">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Скидки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Карта сайта</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О сайте</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            <#if user??>
                <li class="nav-item">
                    <a tabindex="0" class="nav-link" data-toggle="tooltip" title="Войти в личный кабинет" href="/account">${user.firstName}</a>
                </li>

                <li class="nav-item">
                    <form method="post" action="logout">
                        <a href="/logout" class="nav-link" role="button" name="logout">Выйти</a>

                    </form>
                </li>
            <#else>


                <li class="nav-item">
                    <a class="nav-link" href="/login">Войти</a>
                </li>
            </#if>
        </ul>

    </div>
</nav>

<!--
    <#-- <@nav/>-->
    -->
<div class="container space">
    <div class="row">
        <div class="col-lg-9">
            <div class="well">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="../static/img/portfolio_03.jpg" width="240" height="240">
                            </div>
                            <div class="col-lg-7 text-center" style="margin: auto">
                                <h2>${person.firstName} ${person.secondName}</h2>
                                <p><#if person.description??>${person.description}</#if></p>
                            </div>
                        </div>

                        <div class="container">
                            <h4>Услуги и цены</h4>
                            <div class="row">
                                <div class="col-lg-6 text-left">
                                    <#if services?has_content>
                                        <#list services as s>
                                        <p>${s.name}</p>
                                        </#list>
                                    </#if>
                                </div>
                                <div class="col-lg-6 text-right">
                                <#if services?has_content>
                                    <#list services as s>
                                        <p>${s.price}</p>
                                    </#list>
                                </#if>
                                </div>
                            </div>








                          <#--  <h4>Опыт</h4>
                            <p>Опыт работы – с 2012 года</p>-->
                        </div>

                    </div>

                    <div class="col-lg-3 ">
                        <button class="btn btn-lg btn-primary btn-block">Выбрать</button>

                        <div class="space lobster">
                            <h4 class="text-center">Рейтинг 5++</h4>
                            <h4 class="text-center">30 отзывов</h4>
                        </div>
                        <h6><i class="fa fa-check"></i> Работает по договору</h6>
                        <h6><i class="fa fa-check"></i> Даёт гарантию</h6>
                        <h6><i class="fa fa-check"></i> Собеседование пройдено</h6>
                        <h6><i class="fa fa-check"></i> Данные проверены</h6>
                    </div>
                </div>
                <div class="owl-carousel owl-theme">
                    <div class="item"><img src="../static/img/portfolio_01.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_02.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_03.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_04.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_05.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_06.jpg"></div>
                    <div class="item"><img src="../static/img/portfolio_07.jpg"></div>
                </div>


                <div class="container space lobster">
                    <h3 class="text-center">Отзывы</h3>
                    <hr align="center" width="100%" size="1" color="#fafafa"/>
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="../static/img/portfolio_07.jpg" width="90" height="90" class="rounded-circle">
                        </div>
                        <div class="col-lg-10">
                            <h4>Иванов Иван</h4>
                            <h5>Оценка 5</h5>
                        </div>
                        <div style="margin-top: 10px">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia
                                placeat
                                recusandae
                                tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti
                                facere
                                reiciendis voluptate!</p>
                            <h6 class="text-right">Опубликовано 21.10.2017</h6>
                            <hr align="center" width="100%" size="1" color="#fafafa"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2">
                            <img src="../static/img/portfolio_07.jpg" width="90" height="90" class="rounded-circle">
                        </div>
                        <div class="col-lg-10">
                            <h4>Иванов Иван</h4>
                            <h5>Оценка 5</h5>
                        </div>
                        <div style="margin-top: 10px">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias iusto natus officia
                                placeat
                                recusandae
                                tempora temporibus velit. A aliquam eos nostrum quia rerum tempora unde velit. Deleniti
                                facere
                                reiciendis voluptate!</p>
                            <h6 class="text-right">Опубликовано 21.10.2017</h6>
                            <hr align="center" width="100%" size="1" color="#fafafa"/>
                        </div>
                    </div>

                </div>
                <div class="text-center"><a class="btn btn-lg btn-primary" href="#" role="button">
                    Показать ещё</a>
                </div>
            </div>
        </div>

        <div class="col-lg-3 text-center" >
            <div class="well lobster">
                <h3>Возможно вас заинтересуют</h3>
                <div>
                    <#--<hr align="center" width="100%" size="1" color="#fafafa"/>
                    <a href="#"><h3>Хакимов Динар Иршатович</h3></a>
                    <img src="../static/img/portfolio_03.jpg" width="100" height="100">
                    <p style="margin-top: 10px">Мелкий ремонт, сантехника, бытовая техника</p>
                    <div class="lobster">
                        <h4>Рейтинг 100</h4>
                        <h4>25 отзывов</h4>
                    </div>-->
                    <H1>Реклама</H1>
                </div>

                <div>
                   <#-- <hr align="center" width="100%" size="1" color="#fafafa"/>
                    <a href="#"><h3>Хакимов Динар Иршатович</h3></a>
                    <img src="../static/img/portfolio_03.jpg" width="100" height="100">
                    <p style="margin-top: 10px">Мелкий ремонт, сантехника, бытовая техника</p>
                    <div class="lobster">
                        <h4>Рейтинг 100</h4>
                        <h4>25 отзывов</h4>
                    </div>-->
                       <H1>Реклама</H1>
                </div>

                <div>
                    <#--<hr align="center" width="100%" size="1" color="#fafafa"/>
                    <a href="#"><h3>Хакимов Динар Иршатович</h3></a>
                    <img src="../static/img/portfolio_03.jpg" width="100" height="100">
                    <p style="margin-top: 10px">Мелкий ремонт, сантехника, бытовая техника</p>
                    <div class="lobster">
                        <h4>Рейтинг 100</h4>
                        <h4>25 отзывов</h4>
                    </div>-->
                        <H1>Реклама</H1>
                </div>
            </div>
        </div>
    </div>
</div>

<!--
<#--     <@footer/>-->
    -->
<@footer/>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<@scripts/>
<script>
    $('.dropdown-toggle').dropdown();
    $('#divNewNotifications li > a').click(function () {
        $('#text').text($(this).html());
    });
</script>

<script src="../static/owlcarousel/owl.carousel.min.js"></script>

<script>
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        autoplay: true,
        smartSpeed: 1000, //Время движения слайда
        autoplayTimeout: 3000, //Время смены слайда
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            }
        }
    })
</script>
</body>
</html>