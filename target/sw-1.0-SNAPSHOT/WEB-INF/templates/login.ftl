<#include "base.ftl"/>
<!DOCTYPE html>
<html lang="en">

<head>
<@head/>
    <meta pageEncoding="Cp1251">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.w3schools.com/lib/w3.js"></script>
</head>
<body>



<nav class="navbar sticky-top navbar-toggleable-md navbar-light bg-faded" id="navbar1">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li>
                <a class="navbar-brand">
                    <img src="../static/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Profi</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Выберите город
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" name="сity">Казань</a>
                    <a class="dropdown-item" name="сity">Набережные Челны</a>
                    <a class="dropdown-item" name="сity">Альметьевск</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/main">Главная</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Специалисты</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Скидки</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Отзывы</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Карта сайта</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">О сайте</a>
            </li>
        </ul>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link"><img src="../static/img/logo.png" width="30" height="30"
                                         class="rounded-circle" alt=""></a>
            </li>
          <#--  <li class="nav-item">
                <a class="nav-link" href="account.ftl">Иван</a>
            </li>-->
            <li class="nav-item">
                <a class="nav-link" href="/login">Войти</a>
            </li>
          <#--  <li class="nav-item">
                <form method="post">
                    <a class="nav-link" role="button" name="logout">Выйти</a>
                </form>
            </li>-->
        </ul>

    </div>
</nav>

<div class="container" style="min-height: calc(100vh - 207px)">


    <form action="" method="post" class="form-signin">

        <h5 class="form-signin-heading text-center">Пожалуйста, авторизуйтесь</h5>
        <label for="inputLogin" class="sr-only">Login</label>
        <input type="text" id="inputLogin" class="form-control" name="login" placeholder="Логин" required autofocus>

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Пароль" required>
        <#if flag>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            Неправильно введены логин или пароль
        </div>
        </#if>

        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember" value="remember-me"> Запомнить меня
            </label>
        </div>

        <!-- Button trigger modal -->
        <button class="btn btn-lg btn-primary btn-block" type="button" data-toggle="modal" data-target="#myModal">
            Регистрация
        </button>
        <button class="btn btn-lg btn-primary btn-block" type="submit" action="LoginServlet" method="POST">Войти</button>
    </form>
</div> <!-- /container -->

<!-- Modal -->


<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Регистрация</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form action="/registration" method="post" class="form-signin">
                        <script type="application/javascript">
                            var correctLogin = false;
                            var correctPassword = false;
                            function isAllOkay() {
                                if (correctPassword && correctLogin){
                                    $("#errorLog").attr('hidden',true);
                                    $("#regButton").attr('disabled',false);
                                    console.log('oooops');
                                }else{
                                    document.getElementById("p1").innerHTML=""
                                    $("#errorLog").attr('hidden',false);
                                    $("#regButton").attr('disabled',true);
                                    if (!correctLogin){
                                        document.getElementById("p1").innerHTML="-Такой логин уже занят \n"
                                    }
                                    if (!correctPassword){

                                        document.getElementById("p1").innerHTML=document.getElementById("p1").innerHTML + "-Введенные пароли не совпадают";
                                    }
                                }
                            }
                            function checkLogin(){
                                $.ajax({
                                    url: '/check',
                                    type: 'POST',
                                    data:{
                                        login:   $('#regLogin').val()},
                                    dataType: 'json',
                                    success: function (data) {

                                        if (data == false){
                                            console.log("login is free");

                                        }else{
                                            console.log("login is reserved")
                                        }

                                        correctLogin = !data;
                                        isAllOkay();

                                    },
                                    error: function () {
                                        console.log("Login is reserved")
                                    }
                                });
                            }
                            function checkPassword(){
                                /*var p1 = document.getElementById("regPassword");
                                console.log(p1);
                                var p2 = document.getElementById("regPasswordRepeat");
                                console.log(p2);
                                correctPassword = (p1.val() == p2.value);
                                console.log(p1 == p2);*/
                                var p1 = $('#regPassword').val();
                                console.log(p1);
                                var p2 = $('#regPasswordRepeat').val();
                                console.log(p2);
                                correctPassword = (p1 == p2);
                                console.log(p1 == p2);
                                isAllOkay();

                            }
                        </script>

                        <label for="login" class="sr-only">Login</label>
                        <input type="text" id="regLogin" class="form-control" name="regLogin" placeholder="Логин" required oninput="checkLogin()">

                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="regPassword" class="form-control" name="regPassword"
                               placeholder="Пароль" required oninput="checkPassword()">
                        <label for="regPasswordRepeat" class="sr-only">PasswordRepeat</label>
                        <input style="margin-top: -10px" type="password" id="regPasswordRepeat" class="form-control" name="regPasswordRepeat"
                               placeholder="Повторите пароль" required oninput="checkPassword()">

                        <label for="login" class="sr-only">FirstName</label>
                        <input type="text" id="login" class="form-control" name="regFirstname" placeholder="Имя" required>

                        <label for="login" class="sr-only">SecondName</label>
                        <input type="text" id="login" class="form-control" name="regSecondname" placeholder="Фамилия" required>

                        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="errorLog" name="errorLog" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p id="p1">

                            </p>
                            <#--Такой email уже зарегистрирован <br/>
                            Этот логин уже занят другим пользователем <br/>
                            Пароль должен содержать строчные и заглавные латинские буквы, а также цифры <br/>
                            Пароли не совпадают-->
                        </div>


                        <button class="btn btn-lg btn-primary btn-block" type="submit" id="regButton" name="regButton">Зарегистрироваться</button>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" type="button" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<@footer/>


<!-- jQuery first, then Tether, then Bootstrap JS. -->
<#--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
        crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="../static/js/bootstrap.min.js"></script>
</body>
</html>